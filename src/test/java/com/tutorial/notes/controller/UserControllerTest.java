package com.tutorial.notes.controller;

import com.tutorial.notes.entity.User;
import com.tutorial.notes.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void testGetUserFound() throws Exception {

        User mockUser =  new User(1, "xyz", "9876543210", "123");
        doReturn(mockUser).when(userService).getUser(1);

        mockMvc.perform(get("/admin/display-user/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId", is(1)))
                .andExpect(jsonPath("$.userName", is("xyz")))
                .andExpect(jsonPath("$.contactNo", is("9876543210")));

    }

    @Test
    public void testGetUserNotFound() throws Exception {

        doReturn(null).when(userService).getUser(1);

        mockMvc.perform(get("/admin/display-user/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").doesNotExist())
                .andExpect(jsonPath("$.userName").doesNotExist())
                .andExpect(jsonPath("$.contactNo").doesNotExist());

    }

}
