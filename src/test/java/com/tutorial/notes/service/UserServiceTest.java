package com.tutorial.notes.service;

import com.tutorial.notes.entity.User;
import com.tutorial.notes.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void testGetUsersFound() {

        List<User> users = new ArrayList<User>();
        users.add(new User(1, "a", "9999999999", "123"));
        users.add(new User(2, "a", "9999999999", "123"));
        users.add(new User(3, "a", "9999999999", "123"));

        when(userRepository.findAll()).thenReturn(users);

        List<User> result = userService.getUsers();
        assertEquals(3, result.size());

    }

}
