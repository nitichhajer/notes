package com.tutorial.notes.repository;

import com.tutorial.notes.entity.Note;
import com.tutorial.notes.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotesRepository extends JpaRepository<Note, Integer> {

//    todo: find correct query
//    @Query("select n from Note n where user_id = ?1")
//    List<Note> getAllNotes(int userId);

    //all notes of a particular user
    List<Note> findNoteByUser(User user);

    //note of noteId belonging to user userId
    @Query("select n from Note n where note_id = ?1 and user_id = ?2")
    Note findNoteById(int noteId, int userId);

}
