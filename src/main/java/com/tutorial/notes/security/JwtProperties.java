package com.tutorial.notes.security;

public class JwtProperties {

    public static final String SECRET = "Secret";
    public static final int EXPIRATION_TIME = 86400000;    //milliseconds
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

}
