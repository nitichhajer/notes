package com.tutorial.notes.controller;

import com.tutorial.notes.dto.request.UserRequestDTO;
import com.tutorial.notes.entity.User;
import com.tutorial.notes.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/admin")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;

    }

    @GetMapping(value = "/users")
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping(value = "/users/{user_id}")
    public User getUser(@PathVariable("user_id") int userId) {
        return userService.getUser(userId);
    }

    @PostMapping(value = "/users")
    public User createUser(@Valid @RequestBody UserRequestDTO userRequestDTO) {
        return userService.createUser(userRequestDTO);
    }

    @PutMapping(value = "/users/{user_id}")
    public User updateUser(@Valid @RequestBody UserRequestDTO userRequestDTO, @PathVariable("user_id") int userId) {
        return userService.updateUser(userRequestDTO, userId);
    }

    @DeleteMapping(value = "/users/{user_id}")
    public void deleteUser(@PathVariable("user_id") int userId, HttpServletResponse httpServletResponse) {
        userService.deleteUser(userId, httpServletResponse);
    }

}
