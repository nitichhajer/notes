package com.tutorial.notes.controller;

import com.tutorial.notes.dto.request.NoteRequestDTO;
import com.tutorial.notes.entity.Note;
import com.tutorial.notes.service.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user/{user_id}")
public class NotesController {

    private NotesService notesService;

    @Autowired
    public NotesController(NotesService notesService) {
        this.notesService = notesService;
    }

    @GetMapping(value = "/notes")
    public List<Note> getNotes(@PathVariable("user_id") int userId) {
        return notesService.getNotes(userId);
    }

    @GetMapping(value = "/notes/{note_id}")
    public Note getNote(@PathVariable("user_id") int userId, @PathVariable("note_id") int noteId) {
        return notesService.getNote(noteId, userId);
    }

    @PostMapping(value = "/notes")
    public Note createNote(@Valid @RequestBody NoteRequestDTO noteRequestDTO, @PathVariable("user_id") int userId) {
        System.out.println("hi");
        return notesService.createNote(noteRequestDTO, userId);
    }

    @PutMapping(value = "/notes/{note_id}")
    public Note updateNote(@Valid @RequestBody NoteRequestDTO noteRequestDTO, @PathVariable("user_id") int userId, @PathVariable("note_id") int noteId) {
        return notesService.updateNote(noteRequestDTO, userId, noteId);
    }

    @DeleteMapping(value = "/notes/{note_id}")
    public void deleteNote(@PathVariable("user_id") int userId, @PathVariable("note_id") int noteId, HttpServletResponse httpServletResponse) {
        notesService.deleteNote(noteId, userId, httpServletResponse);
    }

}

