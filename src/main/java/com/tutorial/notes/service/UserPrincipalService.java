package com.tutorial.notes.service;

import com.tutorial.notes.entity.User;
import com.tutorial.notes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserPrincipalService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userIdentity) throws UsernameNotFoundException {
        int userId = Integer.parseInt(userIdentity);
        User user = userRepository.findById(userId).orElse(null);
        if(user == null) {
            throw new UsernameNotFoundException("Invalid ID");
        }
        return new UserPrincipal(user);
    }
}
