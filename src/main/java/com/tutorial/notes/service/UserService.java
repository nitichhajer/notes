package com.tutorial.notes.service;

import com.tutorial.notes.dto.request.UserRequestDTO;
import com.tutorial.notes.entity.User;
import com.tutorial.notes.exception.DataNotFoundException;
import com.tutorial.notes.exception.DataIntegrityException;
import com.tutorial.notes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private static final String USER_NOT_FOUND = "User not found";
    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public List<User> getUsers() {
        List<User> users = userRepository.findAll();
        if(users.isEmpty()) {
            throw new DataNotFoundException("No users found");
        }
        return users;
    }

    public User getUser(int userId) {
        User user = userRepository.findById(userId).orElse(null);
        if(user == null) {
            throw new DataNotFoundException(USER_NOT_FOUND);
        }
        return user;
    }

    public User createUser(UserRequestDTO userRequestDTO) {

        User user = new User();

        user.setUserName(userRequestDTO.getUserName());
        user.setContactNo(userRequestDTO.getContactNo());
        user.setPassword(passwordEncoder.encode(userRequestDTO.getPassword()));

        try {
            return userRepository.save(user);
        }

        catch (DataIntegrityViolationException e) {
            throw new DataIntegrityException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }

    }

    public User updateUser(UserRequestDTO userRequestDTO, int userId) {

        User user;

        user = new User(userId, userRequestDTO.getUserName(), userRequestDTO.getContactNo(), passwordEncoder.encode(userRequestDTO.getPassword()));

        //check if user is present in DB
        if (!userRepository.findById(userId).isPresent()) {
            throw new DataNotFoundException(USER_NOT_FOUND);
        }

        try {
            return userRepository.save(user);
        }

        catch (DataIntegrityViolationException e) {
            throw new DataIntegrityException(Objects.requireNonNull(e.getRootCause()).getMessage());
        }


    }

    public void deleteUser(int userId, HttpServletResponse httpServletResponse) {
        //check if user is present in DB
        if (!userRepository.findById(userId).isPresent()) {
            throw new DataNotFoundException(USER_NOT_FOUND);
        }
        userRepository.deleteById(userId);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
    }

}