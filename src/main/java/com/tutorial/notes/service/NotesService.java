package com.tutorial.notes.service;

import com.tutorial.notes.dto.request.NoteRequestDTO;
import com.tutorial.notes.entity.Note;
import com.tutorial.notes.entity.User;
import com.tutorial.notes.exception.DataIntegrityException;
import com.tutorial.notes.exception.DataNotFoundException;
import com.tutorial.notes.repository.NotesRepository;
import com.tutorial.notes.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class NotesService {

    private static final String NOTE_NOT_FOUND = "Note not found";
    private NotesRepository notesRepository;
    private UserRepository userRepository;

    @Autowired
    public NotesService( NotesRepository notesRepository, UserRepository userRepository) {
        this.notesRepository = notesRepository;
        this.userRepository = userRepository;
    }

    public List<Note> getNotes(int userId) {
        User user=userRepository.findUserByUserId(userId);
        if(user == null) {
            throw new DataNotFoundException("User not found");
        }
        List<Note> notes = notesRepository.findNoteByUser(user);
        if(notes.isEmpty()) {
            throw new DataNotFoundException(NOTE_NOT_FOUND);
        }
        return notes;
    }

    public Note getNote(int noteId, int userId) {
        Note note =  notesRepository.findNoteById(noteId, userId);
        if(note == null) {
            throw new DataNotFoundException(NOTE_NOT_FOUND);
        }
        return note;
    }

    public Note createNote(NoteRequestDTO noteRequestDTO, int userId) {

        Note note = new Note();
        User noteUser = new User();

        note.setNoteContent(noteRequestDTO.getNoteContent());
        noteUser.setUserId(userId);
        note.setUser(noteUser);

        try {
            return notesRepository.save(note);
        }

        catch (DataIntegrityViolationException e) {
            throw new DataIntegrityException(e.getRootCause().getMessage());
        }
    }

    public Note updateNote(NoteRequestDTO noteRequestDTO, int userId, int noteId) {
        Note note = new Note(noteId, noteRequestDTO.getNoteContent());
        //check if note of given user is present in DB
        if(notesRepository.findNoteById(noteId, userId) == null) {
            throw new DataNotFoundException(NOTE_NOT_FOUND);
        }

        try {
            return notesRepository.save(note);
        }

        catch (DataIntegrityViolationException e) {
            throw new DataIntegrityException(e.getRootCause().getMessage());
        }

    }


    public void deleteNote(int noteId, int userId, HttpServletResponse httpServletResponse) {
        //check if note of given user is present in DB
        if(notesRepository.findNoteById(noteId, userId) == null) {
            throw new DataNotFoundException(NOTE_NOT_FOUND);
        }
        notesRepository.deleteById(noteId);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
    }
}
