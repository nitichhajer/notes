package com.tutorial.notes.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserRequestDTO {

    @NotNull(message = "Username null")
    @NotBlank(message = "Username empty")
    private String userName;

    @NotNull (message = "Contact null")
    @NotBlank (message = "Contact empty")
    @Pattern(regexp = "^[0-9]{10}")
    private String contactNo;

    @NotNull (message = "Password null")
    @NotBlank (message = "Password empty")
    private String password;

    public UserRequestDTO(String userName, String contactNo, String password) {
        this.userName = userName;
        this.contactNo = contactNo;
        this.password = password;
    }

    public UserRequestDTO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
