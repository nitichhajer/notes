package com.tutorial.notes.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class NoteRequestDTO {

    @NotNull(message = "Note null")
    @NotBlank (message = "Note empty")
    private String noteContent;

    public NoteRequestDTO() {
    }

    public NoteRequestDTO(String noteContent) {
        this.noteContent = noteContent;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }
}
